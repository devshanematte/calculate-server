
import json from 'koa-json';
import cors from 'koa2-cors';
import niv from 'node-input-validator';
import bodyParser from 'koa-bodyparser';
import koaBody from 'koa-body';
import logger from 'koa-logger';

const init = (app, routes) => {

  app.use(cors());

  app.use(koaBody({
    multipart: true,
    uploadDir: '.'
  }));

  app.use(niv.koa());
  app.use(json());
  app.use(bodyParser());

  app.use(logger());
  app.use(routes);

}

export default init;
