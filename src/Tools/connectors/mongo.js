import mongoose from 'mongoose';
import config from 'config';

const mongoURL = `mongodb://${config.DB.host}:${config.DB.port}/${config.DB.db_name}`;
 
mongoose.connect(mongoURL, {
  auth: {
    username: config.DB.user,
    password: config.DB.pwd
  },
  autoIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true
});

export default mongoose;