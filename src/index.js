import config from 'config';
import http from 'http';
import app from './app';

const PORT = config.get('DEFAULT_SERVER_PORT');

const createServer = http.createServer(app.callback());

createServer.listen(PORT, () => {
  console.log(`started server on port ${PORT}`)
});