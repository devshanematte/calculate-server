import Koa from 'koa';
import routes from './controllers/index';
import { init } from './Tools';

const app = new Koa();

init(app, routes);

export default app;
