import Router from 'koa-router';

//routes
import apiV1 from './routes/v1';

const router = new Router();

router.use(apiV1);

export default router.routes();