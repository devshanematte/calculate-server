import {
	calculate,
  summary
} from '../../../../../models';
import calc from './helpers';

const Calc = new calc();

export default {
  saveCalculate: async (ctx) => {

    try {

      let {
        ipn,
        opv,
        osms,
        vosms,
        co,
        salary_with_tax,
        salary
      } = Calc.total(ctx.fields.salary, ctx.fields.tax_credit, ctx.fields.retired, ctx.fields.handicapped, ctx.fields.handicapped_group)

      let totalSave = await summary.create({
        ipn,
        opv,
        osms,
        vosms,
        co,
        salary_with_tax
      });

      let create = await calculate.create({
        salary:ctx.fields.salary,
        work_time:ctx.fields.work_time,
        worked_out:ctx.fields.worked_out,
        tax_credit:ctx.fields.tax_credit,
        year:ctx.fields.year,
        month:ctx.fields.month,
        handicapped:{
          status:ctx.fields.handicapped,
          group:ctx.fields.handicapped_group
        },
        summary: totalSave._id
      });

      let getSaveCalc = await calculate.findOne({
        _id:create._id
      }).lean().deepPopulate('summary');

      ctx.status = 200;
      return ctx.body = getSaveCalc;

    } catch(err) {
      ctx.status = 500;
      return ctx.body = 'Error';

    }

  },
  getCalculate: async (ctx) => {

    let {
      salary,
      work_time,
      worked_out,
      tax_credit,
      year,
      month,
      retired,
      handicapped,
      handicapped_group
    } = ctx.fields;

    try {

      ctx.status = 200;
      return ctx.body = Calc.total(salary, tax_credit, retired, handicapped, handicapped_group);

    } catch(err) {
      ctx.status = 500;
      return ctx.body = 'Error';

    }

  }
}