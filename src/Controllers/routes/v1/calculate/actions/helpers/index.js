/*
Зарплата облагается следующими налогами:
1) Индивидуальный подоходный налог (ИПН)
2) Обязательные пенсионные взносы (ОПВ)
3) Обязательное социальное медицинское страхование (ОСМС)
4) Взносы на обязательное социальное медицинское Страхование (ВОСМС)
5) Социальные отчисления (СО)
Данные, которые должен вернуть API: Налоги (выше перечисленные), зарплата на руки (сумма за вычетом налогов, которые удерживаются с сотрудника), начисленная зарплата.
Формулы и условия расчёта значений:

ИПН = ЗП – ОПВ – 1МЗП (при наличии вычета) – ВОМСМ – Корректировка (при наличии, об этом ниже) * 10%;

Если заработная плата за месяц меньше 25 МРП, срабатывает корректировка; 
Расчет корректировки (ЗП - ОПВ - 1МЗП (при наличии вычета) - ВОСМС) * 90%:

ОПВ = ЗП * 10%;

ВОСМС = ЗП * 2%;

ОСМС = ЗП * 2%;

СО = (ЗП-ОПВ) * 3,5%

Пенсионер облагается лишь ИПН;
Пенсионер с инвалидностью не облагается налогами;
Инвалид 1 и 2 группы облагается лишь СО;
Инвалид 3 группы облагается ОПВ и СО;
Если ЗП у инвалида превысила 882 МРП, он облагается ИПН;
Всё расчёты производятся для сотрудника ИП с трудовым договором.
*/

class calc {

	constructor(){
		this.mzp = 42500;
		this.mrp = 2917;
	}

	co(salary){
		return (salary - this.opv(salary)) * 0.35;
	}

	vosms(salary){
		return salary * 0.02;
	}

	osms(salary){
		return salary * 0.02;
	}

	general_tax(salary, tax_credit){
		return this.ipn(salary, tax_credit) + this.opv(salary) + this.osms(salary) + this.vosms(salary) + this.co(salary);
	}

	retired_tax(salary, tax_credit){
		return this.ipn(salary, tax_credit) + this.opv(salary) + this.osms(salary) + this.vosms(salary) + this.co(salary);
	}

	correcting(salary, tc){

		let TFMRP = this.mrp * 25;
		let result = 0;

		if(salary < TFMRP){
			result = (salary - this.opv(salary) - tc - this.vosms(salary)) * 0.9;
		}

		return result;
	}

	tc(tax_credit){
		return tax_credit ? 0 : this.mzp;
	}

	ipn(salary, tax_credit){

		return (salary - this.opv(salary) - this.tc(tax_credit) - this.vosms(salary) - this.correcting(salary, this.tc(tax_credit))) * 0.1;

	}

	opv(salary){
		return salary * 0.1;
	}

	total(salary, tax_credit, retired, handicapped, handicapped_group){

		let res = {
	        ipn: this.ipn(salary, tax_credit),
	        opv: this.opv(salary),
	        osms: this.osms(salary),
	        vosms: this.vosms(salary),
	        co: this.co(salary),
	        salary_with_tax: salary - this.general_tax(salary, tax_credit),
	        salary
      	}

		if(retired && handicapped){
			res.salary_with_tax = salary;
			return res;
		}

		if(retired){

			if(salary > (this.mrp * 882)){
				res.salary_with_tax = salary - this.ipn(salary, tax_credit);
			}else{
				res.salary_with_tax = salary - this.ipn(salary, tax_credit);
			}

			return res;

		}

		if(handicapped && salary > (this.mrp * 882)){
			res.salary_with_tax = salary - this.ipn(salary, tax_credit);
			return res;
		}

		if(handicapped_group == 3){

			res.salary_with_tax = salary - this.opv(salary) - this.co(salary);
			return res;
		}

		if(handicapped_group == 1 || handicapped_group == 2){
			res.salary_with_tax = salary - this.co(salary);
			return res;
		}

		return res;

	}

}

export default calc;

