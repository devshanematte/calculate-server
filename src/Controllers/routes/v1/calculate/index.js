import Router from 'koa-router';
import actions from './actions';
import validation from './validation'

const router = new Router();

router.get('/calculate',
  validation.calculateFields,
  actions.getCalculate
);

router.post('/calculate',
  validation.calculateFields,
  actions.saveCalculate
);

export default router.routes();