
export default {
  calculateFields: async (ctx, next) => {

    const v = await ctx.validator(ctx.query, {
      salary: 'required',
      work_time: 'required',
      worked_out: 'required',
      tax_credit: 'required',
      year: 'required',
      month: 'required',
      retired: 'required',
      handicapped: 'required',
      handicapped_group: 'required'
    });
    
    if (await v.fails()) {
      ctx.status = 422;
      return ctx.body = v.errors;
    }

    ctx.fields = {
      retired: Boolean(Number(ctx.query.retired)),
      handicapped: Boolean(Number(ctx.query.handicapped)),
      month: Number(ctx.query.month),
      tax_credit: Boolean(Number(ctx.query.tax_credit)),
      year: Number(ctx.query.year),
      worked_out: Number(ctx.query.worked_out),
      work_time: Number(ctx.query.work_time),
      salary: Number(ctx.query.salary),
      handicapped_group: Number(ctx.query.handicapped_group),
    }

    return next();

  }
}