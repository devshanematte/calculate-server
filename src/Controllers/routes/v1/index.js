import Router from 'koa-router';
import calculateRoutes from './calculate';

const router = new Router({
	prefix: '/v1'
});

router.use(calculateRoutes);

export default router.routes();