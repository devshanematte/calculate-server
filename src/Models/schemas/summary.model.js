import { mongoose } from '../../Tools';

const summarySchema = new mongoose.Schema({
	ipn: {
		type: Number,
		default: 0
	},
  opv: {
    type: Number,
    default: 0
  },
  osms: {
    type: Number,
    default: 0
  },
  vosms: {
    type: Number,
    default: 0
  },
  co:{
    type: Number,
    default: 0
  },
  salary_with_tax:{
    type: Number,
    default: 0
  }
},{
  timestamps: true
});

const summary = mongoose.model('summary', summarySchema);

export default summary;