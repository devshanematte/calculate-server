import { mongoose } from '../../Tools';
import deepPopulate from 'mongoose-deep-populate';

const calculateSchema = new mongoose.Schema({
	salary: {
		type: Number,
		default: 0
	},
  work_time: {
    type: Number,
    default: 0 //Days
  },
  worked_out: {
    type: Number,
    default: 0 //Days
  },
  tax_credit: {
    type: Boolean,
    default: false
  },
  year:{
    type: Number,
    default: 0
  },
  month:{
    type: Number,
    default: 0
  },
  retired: {
    type: Boolean,
    default: false
  },
  handicapped: {
    status: {
      type: Boolean,
      default: false
    },
    group:{ //1,2,3
      type: Number
    }
  },
  summary: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'summary' 
  }
},{
  timestamps: true
});

calculateSchema.plugin(deepPopulate(mongoose));

const calculate = mongoose.model('calculate', calculateSchema);

export default calculate;